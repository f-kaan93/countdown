//
//function and variable declarations
//


var counter;
var intervalHandler;
var errorHandler;

function setMessage(yourMessage) {
    $("#messageSpan").html(yourMessage).fadeIn(600);
    function resetMessage () {
        $("#messageSpan").fadeOut(600);
    };
    setTimeout(resetMessage, 3000);
}

function resetPage(){
    clearInterval(intervalHandler);
    $("#minutesInput").val("");
    $("#secondsInput").val("");
    if($("#textSpan").html() !== "0:00"){
    $("#textSpan").fadeOut(300, function(){
      $("#textSpan").html("0:00");
      $("#textSpan").fadeIn(300);
    });
    }
}

function getInput () {
    var minutesInput = $("#minutesInput").val();
    var secondsInput = $("#secondsInput").val();
    counter = Number(minutesInput*60) + Number(secondsInput);
//  error check

    if(isNaN(minutesInput) || isNaN(secondsInput)) {
        resetPage();
        setMessage("Please enter something useful")
        errorHandler = false;
        return;
    } else if (minutesInput === "" && secondsInput === "") {
        resetPage();
        errorHandler = false;
        return;

    } else if (minutesInput == 0 && secondsInput == 0) {
        resetPage();
        setMessage("Well, that was quick!")
        errorHandler = false;
        return;
    } else if (counter < 0) {
        resetPage();
        setMessage("&#8734;")
        errorHandler = false;
        return;
    }
//  return global time variable

    return counter;
}

function tick () {

 // takes global time variable and converts it to more understandable
 // seperate minutes seconds


    var minutesOutput = counter / 60;
    minutesOutput = Math.floor(minutesOutput);

    var secondsOutput = counter % 60;

    if (secondsOutput < 10) {
        secondsOutput = "0" + secondsOutput;
    }
    if (counter < 6) {
        $("#textSpan").fadeTo(200, 0.3, function(){
             $(this).html(minutesOutput + ":" + secondsOutput).fadeTo(200, 1);
        });
    } else {
        $("#textSpan").html(minutesOutput + ":" + secondsOutput);
    }

    if (counter == 0) {
        clearInterval(intervalHandler);
        $("#minutesInput").val("");
        $("#secondsInput").val("");
        setMessage("Done!!");
        return;
    }
    counter--;
}


//
// script starts here
//




$("#startButton").click(function () {


    errorHandler = true;
//  ^  i think i should come up with more elegant solution than this

    clearInterval(intervalHandler);

    getInput();
    if (errorHandler == false){
        return;
    }
    tick();
    intervalHandler = setInterval(tick, 1000);
});

//self explainatory

$("#resetButton").click(function () {
    resetPage();
});

$("#secondsInput").keyup(function(event){
    if(event.keyCode == 13){
        $("#startButton").click();
    }
});

$("#minutesInput").keyup(function(event){
    if(event.keyCode == 13){
        $("#startButton").click();
    }
});

setMessage("A simple countdown timer")
